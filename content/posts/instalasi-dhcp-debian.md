---
title: "Instalasi DHCP server di Debian"
date: 2021-11-25T11:23:31+07:00
draft: false
toc: false
images:
tags:
  - untagged
---

Langkah Langkah Instalasi DCHP (Dynamic Configuration Host Protocol) di Debian:

**1. Update repository debian**
{{< highlight bash >}}
apt update
apt upgrade #opsional
{{< /highlight >}}
**2. Install package isc-dhcp-server dari repository debian**
{{< highlight bash >}}
apt install isc-dhcp-sever
{{< /highlight >}}

**3. Konfigurasi dhcp server**  

Kita ubah konfigurasi file dhcp server di /etc/dhcp/dhcpd.conf
{{< highlight bash >}}
 vim /etc/dhcp/dhcpd.conf
{{< /highlight >}}

{{< highlight bash >}}
 49 # A slightly different configuration for an internal subnet.
 50 subnet 192.168.16.0 netmask 255.255.255.0 {
 51   range 192.168.16.40 192.168.16.50;
 52   option domain-name-servers 8.8.8.8;
 53   option domain-name "internal.example.org";
 54   option routers 192.168.16.1;
 55   option broadcast-address 192.168.16.255;
 56   default-lease-time 600;
 57   max-lease-time 7200;
 58 }
{{< /highlight >}}

_subnet_	  = network address  
_netmask_	  = subnet mask  
_range_		  = jangkauan ip address dhcp  
_option domain_	  = DNS server  
_option routers_  = IP router / gateway  
_option broadcast_ = IP broadcast  

**4. Konfigurasi interface untuk DHCP**  
{{< highlight bash >}}
vim /etc/default/isc-dhcp-server
{{< /highlight>}}

{{< highlight bash >}}
17 INTERFACESv4="enp1s0"
18 INTERFACESv6=""
{{< /highlight >}}

**5. Restart DHCP service**
{{< highlight bash >}}
systemctl restart isc-dhcp-server
{{< /highlight >}}

**6. Pengujian Client**

Pengujian dari client menggunakan linux desktop  
Perintah yang digunakan:

Killed old client process / menghapus konfigurasi DHCP saat ini  
{{< highlight bash >}}
sudo dhclient -r enp1s0
{{< /highlight >}}

Memperbarui DHCP client
{{< highlight bash >}}
sudo dhclient enp1s0
{{< /highlight >}}


